
# Merge in geographic IDs
load("data/processed/new_values.rda")
load("data/raw/big/geo.rda")

geo$PROPERTY_ZIP <- substring(geo$PROPERTY_ZIP,1,5)
new_values <- merge(new_values, geo, by.x=c("pin10"), by.y=('PIN'), all.x=TRUE)


# Unemployment with contextual fields
# Census tracts don't line up with CCAO neighborhoods and towns
# I'm NOT going to do a weighted average because that is a level of abstraction that the user should choose for themselves
summary <- assessments %>% dplyr::group_by(NBHD,`TOWN NAME`,census_tract) %>%
  summarise(`Total Properties` = n())

summary <- merge(summary, tract_data, by.x=c('census_tract'), by.y=c('TRACTA'))[,c(1:7, 9:15)]
write.csv(summary, file = "data/processed/unemployment_by_tract&town.csv")


# Summmary by township ----
summary <- new_values %>% dplyr::group_by(triad, `TOWN NAME`, group) %>%
  summarise(
    `Total Properties` = n()
    , `Median COVID Adjustment` =  median(`Avg NBHD COVID-19 Adjustment`, na.rm = TRUE)
    , `Post-COVID 10th Percentile Adjustment` =  quantile(`Avg NBHD COVID-19 Adjustment`, c(.1), names = FALSE, na.rm = TRUE)
    , `Post-COVID 90th Percentile Adjustment` =  quantile(`Avg NBHD COVID-19 Adjustment`, c(.9), names = FALSE, na.rm = TRUE)
  )


write.csv(summary, file = "data/processed/summary_by_town.csv")

# Summmary by neighborhoodtownship ----
new_values %>% dplyr::group_by(triad, TOWN, `TOWN NAME`, NBHD, group) %>%
  summarise(
    `Total Properties` = n()
    , `COVID Adjustment` =  median(`Avg NBHD COVID-19 Adjustment`, na.rm = TRUE)
    , `Post-COVID 10th Percentile Adjustment` =  quantile(`Avg NBHD COVID-19 Adjustment`, c(.1), names = FALSE, na.rm = TRUE)
    , `Post-COVID 90th Percentile Adjustment` =  quantile(`Avg NBHD COVID-19 Adjustment`, c(.9), names = FALSE, na.rm = TRUE)
    , `Pre-COVID Estimated Unemployment Rate` = mean(`Pre-COVID Unemployment Rate`, na.rm = T)
    , `Post-COVID Estimated Unemployment Rate` = mean(`Post-COVID Unemployment Rate`, na.rm = T)
    , `Pre-COVID Median Reference Value` = median(`REFERENCE VALUE`, na.rm = T)
    , `Post-COVID Median Reference Value` = median(`Adjusted Value`, na.rm = T)
  ) %>%
write.csv(file = "data/processed/summary_by_nbhd.csv")

# Summmary by zip code ----
summary <- new_values %>% dplyr::group_by(group, PROPERTY_ZIP) %>%
  summarise(
    `Total Single-Family and Multi-Family Properties` = n()
    , `COVID Adjustment` =  median(`Avg NBHD COVID-19 Adjustment`, na.rm = TRUE)
    , `NBHD Pre-COVID Unemployment` =  mean(`Pre-COVID Unemployment Rate`, na.rm = TRUE)
    , `NBHD Post-COVID Unemployment` =  mean(`Post-COVID Unemployment Rate`, na.rm = TRUE)
  )


write.csv(summary, file = "data/processed/summary_by_zip.csv")

# Summmary by zip code and ward ----
summary <- new_values %>% dplyr::group_by(group, PROPERTY_ZIP, ward) %>%
  summarise(
    `Total Single-Family and Multi-Family Properties` = n()
    , `COVID Adjustment` =  median(`Avg NBHD COVID-19 Adjustment`, na.rm = TRUE)
    , `NBHD Pre-COVID Unemployment` =  mean(`Pre-COVID Unemployment Rate`, na.rm = TRUE)
    , `NBHD Post-COVID Unemployment` =  mean(`Post-COVID Unemployment Rate`, na.rm = TRUE)
  )


write.csv(summary, file = "data/processed/summary_by_ward.csv")

# Summmary by zip code and commissioner district ----
summary <- new_values %>% dplyr::group_by(group, PROPERTY_ZIP, commissioner_dist) %>%
  summarise(
    `Total Single-Family and Multi-Family Properties` = n()
    , `COVID Adjustment` =  median(`Avg NBHD COVID-19 Adjustment`, na.rm = TRUE)
    , `NBHD Pre-COVID Unemployment` =  mean(`Pre-COVID Unemployment Rate`, na.rm = TRUE)
    , `NBHD Post-COVID Unemployment` =  mean(`Post-COVID Unemployment Rate`, na.rm = TRUE)
  )


write.csv(summary, file = "data/processed/summary_by_commdist.csv")

# Summmary by zip code and state rep district ----
summary <- new_values %>% dplyr::group_by(group, PROPERTY_ZIP, reps_dist) %>%
  summarise(
    `Total Single-Family and Multi-Family Properties` = n()
    , `COVID Adjustment` =  median(`Avg NBHD COVID-19 Adjustment`, na.rm = TRUE)
    , `NBHD Pre-COVID Unemployment` =  mean(`Pre-COVID Unemployment Rate`, na.rm = TRUE)
    , `NBHD Post-COVID Unemployment` =  mean(`Post-COVID Unemployment Rate`, na.rm = TRUE)
  )


write.csv(summary, file = "data/processed/summary_by_repsdist.csv")

# Summmary by zip code and state sen district ----
summary <- new_values %>% dplyr::group_by(group, PROPERTY_ZIP, senate_dist) %>%
  summarise(
    `Total Single-Family and Multi-Family Properties` = n()
    , `COVID Adjustment` =  median(`Avg NBHD COVID-19 Adjustment`, na.rm = TRUE)
    , `NBHD Pre-COVID Unemployment` =  mean(`Pre-COVID Unemployment Rate`, na.rm = TRUE)
    , `NBHD Post-COVID Unemployment` =  mean(`Post-COVID Unemployment Rate`, na.rm = TRUE)
  )

write.csv(summary, file = "data/processed/summary_by_sendist.csv")